/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author thn055
 *
 */
public class Fahrenheit extends Temperature
{
public Fahrenheit(float t)
{
super(t);
}
public String toString()
{
	

return this.getValue() + " C";

}
@Override
public Temperature toCelsius() {
	Float value = this.getValue();
	Float celsius = (value -32)*5/9;
	Fahrenheit t = new Fahrenheit(celsius);
	return t;
}
@Override
public Temperature toFahrenheit() {
	Float value = this.getValue();
	Fahrenheit t = new Fahrenheit(value);
	return t;
}
	
}
