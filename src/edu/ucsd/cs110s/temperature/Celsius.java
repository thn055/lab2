/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author thn055
 *
 */
public class Celsius extends Temperature
{
public Celsius(float t)
{
super(t);
}
public String toString()
{


return this.getValue() + "F";

}
@Override
public Temperature toCelsius() {
	Float value = this.getValue();
	Celsius t = new Celsius(value);
	return t;
}
@Override
public Temperature toFahrenheit() {
	Float value = this.getValue();
	Float fahrenheit = (value*9)/5 + 32;
	Celsius t = new Celsius(fahrenheit);
	return t;
}
}
